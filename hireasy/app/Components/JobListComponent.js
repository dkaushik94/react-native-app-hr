/*
	Component for listing Open job positions.
*/

const uid = () => Math.random().toString(34).slice(2);


import React , {Component} from 'react';
import 	{
	StyleSheet,
	TouchableOpacity,
	View,
	Alert,
	ScrollView
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons'
import { Card, CardItem, List, ListItem, Badge, Button, H2, H3, Text } from 'native-base'
import { GlobalFunction } from '../globalAssets.js'
import Candidate from './candidateComponent.js'
import { Actions } from 'react-native-router-flux'

var response = {
  "count": 3,
  "next": null,
  "previous": null,
  "results": [
    {
      "id": "ee8d955f-0210-425a-899d-ed51af2c15687",
      "first_name": "Saksham",
      "last_name": "Verenkar",
      "email": "verenkar.saksham@gmail.com",
      "resume": "https://noldevmedia.s3.amazonaws.com/candidate/201701171513111484665991.jpg",
      "company": "Grappus",
      "open_job": "05822011-fd94-48bd-bf44-cde28594e3cc",
      "created_at": "2017-01-06T08:49:10.459751Z"
    },
    {
      "id": "003bca07-197f-4f8e-9864-57c618a564d3",
      "first_name": "Debojit",
      "last_name": "Kaushik",
      "email": "kaushik.debojit@gmail.com",
      "resume": "https://noldevmedia.s3.amazonaws.com/candidate/201701171510261484665826.png",
      "company": "Grappus",
      "open_job": "05822011-fd94-48bd-bf41-cde28594e3cc",
      "created_at": "2017-01-06T08:50:13.627624Z"
    },
    {
      "id": "ce4e4bc7-5ec1-4971-9e98-b6cab4aa6f16",
      "first_name": "Amritanshu",
      "last_name": "Dwivedi",
      "email": "amritanshu@grappus.com",
      "resume": "https://noldevmedia.s3.amazonaws.com/candidate/201611281538421480347522.py",
      "company": "Grappus",
      "open_job": "05822011-fd94-48bd-bf41-cde28594e3cc",
      "created_at": "2016-11-28T15:38:42.995652Z"
    }
  ]
}


//Component for a Job.
export default class Job extends Component{
	constructor(props){
		super(props)
		this.state = {
			componentData: [],
		}
	}

	render(){
		return(
			<ScrollView>
			<View>
				<Card style={ styles.card }>
					<CardItem heading style={{backgroundColor:'#F0F0F0'}}>
						<H2>{this.props.id}</H2>
					</CardItem>
					<CardItem>
						<Text>DESCRIPTION:</Text>
						<Text>
						New Business Development

						Prospect for potential new clients and turn this into increased business.
						Cold call as appropriate within your market or geographic area to ensure a robust pipeline of opportunities. * Meet potential clients by growing, maintaining, and leveraging your network.
						Identify potential clients, and the decision makers within the client organization.
						The primary role of the Business Development Manager is to prospect for new clients by networking, cold calling, advertising or other means of generating interest from potential clients. They must then plan persuasive approaches and pitches that will convince potential clients to do business with the company.They must develop a rapport with new clients, and set targets for sales and provide support that will continually improve the relationship. They are also required to grow and retain existing accounts by presenting new solutions and services to clients. Business Development Managers work with mid and senior level management, marketing, and technical staff.He/she may manage the activities of others responsible for developing business for the company. Strategic planning is a key part of this job description, since it is the business manager’s responsibility to develop the pipeline of new business coming into the company. This requires a thorough knowledge of the market, the solutions/services the company can provide, and of the company’s competitors.While the exact responsibilities will vary from company to company, the main duties of the Business Development Manager can be summarized as follows:
						Research and build relationships with new clients.
						Set up meetings between client decision makers and company’s practice leaders/Principals.
						Plan approaches and pitches. * Work with team to develop proposals that speaks to the client’s needs, concerns, and objectives.
						</Text>
					</CardItem>
				</Card>
				</View>
				<View>
					<List style={{marginTop: 15, marginBottom:15}}>
						<View style={{flexDirection:'row', justifyContent: 'space-between' , paddingRight:15, marginTop:5, marginBottom:5}}>
							<H3 style={{marginLeft:15}}>CANDIDATES</H3>
							<TouchableOpacity>
								<Icon color="#000000" name="add-circle-outline" size={25} onPress={()=>{
									GlobalFunction({sceneName: 'AddCandidate'})
								}} />
							</TouchableOpacity>
						</View>
					{
						response.results.map((candidate) => {
							return(
								<ListItem key={candidate.id} button onPress={() => {
									GlobalFunction({
										"candidateId":candidate.id, "sceneName":"CandidateDetails"
									})
								}
							}>
							<Text>{candidate.first_name+" "+candidate.last_name}</Text>
							<Badge style={{backgroundColor:'#008000'}}>Applied</Badge>
							</ListItem>
						)
					})
				}
				</List>
			</View>
			</ScrollView>
		)
	}
}


const styles = StyleSheet.create({
	job:{
		color: '#FFFFFF',
		margin: 20
	},
	button:{
		width: 20,
	},
	candidateListContainer:{
		marginBottom:40,
	},
	List:{
		marginLeft:15,
		marginRight:15
	},
	card:{
    margin: 15
  },
	subheading:{
		marginTop:5,
		marginBottom:5
	},
	jobTitle:{
		fontSize: 25,
		marginBottom:10,
		paddingLeft:15,
	},
	description:{
		marginBottom : 10,
		textAlign: 'justify',
		lineHeight: 25,
	},
})
