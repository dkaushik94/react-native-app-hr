import React , { Component } from 'react';

import
{
  Image,
  View,
  StyleSheet,
  ScrollView,
} from 'react-native';

import { Subtitle,Text,LightBox } from '@shoutem/ui'
import { Video } from '@shoutem/ui/components/Video'

export default class Candidate extends Component{
  render(){
    // }
    // let url = {
    //   'uri':'http://aspireid.com/wp/wp-content/uploads/sample-customer-photo2.jpg',
    return(
      <View style={{"marginBottom":20}}>
        <Text>{this.props.name}</Text>
        <Text>{this.props.company}</Text>
      </View>
    );
  }
}



class VideoResponse extends Component{
    componentDidMount(){
      console.log("Component did mount.")
    }
    render(){
      const style ={
        container:{
          alignItems: 'center',
          margin: 20,
        }
      }
      return(
            <Video
              source={{uri:'https://www.youtube.com/watch?v=AWIrK4PDAJ0'}}
              height={330}
              width={330}
              style={style}
            />
        )
    }
}


styles=StyleSheet.create({
  subtitles:{
    marginTop:10,
  },
})

module.exports = {
  Candidate,
  VideoResponse
}
