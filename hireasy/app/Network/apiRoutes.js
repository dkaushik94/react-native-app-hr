// API routes.
//
// Domain URL
const URLSUFFIX = 'http://52.204.174.235/api/v1'

//API ROUTES.
const APIROUTES = {
  'JobsRoute' : '/open_jobs',
}


module.exports = {
  URLSUFFIX,
  APIROUTES
}
