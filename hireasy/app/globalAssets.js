import React , { Component } from 'react'
import {
  View,
  ToastAndroid
} from 'react-native'

//Package Imports
import { Actions , Scene } from 'react-native-router-flux'
import { Spinner } from 'native-base'

import { JobListing,JobDetail } from './Views/OpenJobs.js'
import { URLSUFFIX, APIROUTES } from './Network/apiRoutes.js'



function GlobalFunction(params){
  if(params.sceneName == 'JobDetails')
  {
    Actions.JobDetails({jobId : params.jobId});
  }
  else if(params.sceneName == 'CandidateDetails')
  {
    Actions.CandidateDetails({candidateId : params.candidateId})
  }
  else if(params.sceneName == "AddJob")
  {
    Actions.AddJob()
  }
  else if(params.sceneName == "AddCandidate")
  {
    Actions.AddCandidate()
  }
}



//API data fetch section.


//Function to convert query dictionary into GET params.
function convertParameters(params){
  let arr = [], st
  Object.entries(params).map(([k,v])=>{
    st = k+'='+v
    arr.push(st)
  })
  return arr.join('&')
}

//Data fetching function.
// params: method, queryParameters, componentRef : reference pointer of the component whose state is to be set.
const getData = (componentRef , params) => {
  if(params.method == 'GET')
  {
      let queryStr = convertParameters(params.queryParameters)
      let url = URLSUFFIX+APIROUTES[params.routeSuffix]+'?'+queryStr

      fetch(url,{
        method : 'GET',
    		headers : {
    			'Accept': 'application/json',
    			'Content-Type':'application/json',
          		'Authorization': 'Token 171a2e1181b401fe398d376be610fd2c80369f6a'
    		},
    	})
      .then((res)=>{
        if(res.ok){
          return res
        }
        else{
          ToastAndroid.showWithGravity(
             'We had trouble getting a reponse from the server!',
              ToastAndroid.LONG,
              ToastAndroid.CENTER,
          )
        }
      })
      .then((res)=>{
        return res.json()
      })
      .then((res)=>{
        componentRef.setState({
          dataStream: res.data
        })
      })
      .catch((err)=>{
        console.log(err)
      })
  }
}


//Common Components

//
class ActivityIcon extends Component{
  render(){
    return(
      <View style={{alignItems:'center'}}>
        <Spinner size={50} />
      </View>
    )
  }
}





module.exports = {
  GlobalFunction,
  getData,
  ActivityIcon,
};
