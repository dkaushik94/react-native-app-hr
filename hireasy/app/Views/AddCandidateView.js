import React , { Component } from 'react'
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
} from 'react-native'

import { Container, Header, Content, Card, CardItem, InputGroup, Input, Button, Picker, Title, Icon } from 'native-base'
import { Actions } from 'react-native-router-flux'

export class AddCandidate extends Component{
  render(){
    return(
      <Container style={{flex: 1}}>
        <Header style={{ backgroundColor: "#000000" }}>
          <Button transparent>
            <TouchableOpacity onPress={()=>{
                Actions.pop()
                }
              }
            >
              <Icon name='ios-arrow-back' style={{color: 'white'}} />
            </TouchableOpacity>
          </Button>
          <Title>
            ADD CANDIDATE
          </Title>
        </Header>
        <Content>
          <Card>
            <CardItem>
                <InputGroup borderType='underline'>
                  <Input placeholder='Name Of Candidate' />
                </InputGroup>
            </CardItem>
            <CardItem>
                <InputGroup borderType='underline'>
                  <Input placeholder='Email' />
                </InputGroup>
            </CardItem>
            <CardItem>
                <InputGroup borderType='underline'>
                  <Input placeholder='Name Of Candidate' />
                </InputGroup>
            </CardItem>
          </Card>
        </Content>
        <Button block success style={{height: 60}}>SUBMIT</Button>
      </Container>
    )
  }
}

module.exports = {
  AddCandidate
}
