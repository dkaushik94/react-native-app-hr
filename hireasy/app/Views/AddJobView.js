import React , { Component } from 'react';``
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
} from 'react-native'
import { Actions } from 'react-native-router-flux'
import { Button, Header, Title, Icon, Card, CardItem, InputGroup, Input, Picker} from 'native-base'


const Item = Picker.Item


export default class AddJob extends Component{
  constructor(props){
    super(props);
    this.state = {
      type : 'key1'
    }
  }


  onValueChange(data){
    this.setState({jobType:data})
  }

  render(){
    return(
      <View style={{flex: 1, backgroundColor: '#F0F0F0'}}>
        <Header style={{ backgroundColor: "#000000" }}>
        <Button transparent>
          <TouchableOpacity onPress={()=>{
              Actions.pop()
              }
            }
          >
            <Icon name='ios-arrow-back' style={{color: 'white'}} />
          </TouchableOpacity>
        </Button>
          <Title>ADD JOB</Title>
        </Header>
        <ScrollView>

          <Card style={{marginTop: 80, marginLeft:15, marginRight:15 }}>

            <CardItem>

              <InputGroup borderType='underline' >
                <Input autoCapitalize='sentences' placeholder='Job Title' />
              </InputGroup>

            </CardItem>
            <CardItem>

              <Picker selectedValue={this.state.jobType} onValueChange = {this.onValueChange.bind(this)} mode='dropdown'>

                <Item label="Sales" value='key0' />
                <Item label="Technology" value='key1' />
                <Item label="Business" value='key2' />
                <Item label="Data Analysis" value='key3' />
                <Item label="Design" value='key4' />
                <Item label="Other" value='key5' />

              </Picker>

            </CardItem>
            <CardItem>

              <InputGroup borderType='underline' >
                <Input placeholder='Job Description' multiline={ true } autoCapitalize = 'sentences' numberOfLines={10} style={{height:100}} />
              </InputGroup>

            </CardItem>

          </Card>

      </ScrollView>

      <Button block success style={{height: 60}}>SUBMIT</Button>

      </View>
    )
  }
}




module.exports = {
  AddJob
}
