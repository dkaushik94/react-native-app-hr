import React , { Component } from 'react';
import
{
  Text,
  TouchableOpacity,
  ScrollView,
  View,
  StyleSheet,
} from 'react-native';
import { SocialIcon } from 'react-native-elements'
import {  Button, Container, Card, CardItem, Header, Icon, Title } from 'native-base'
import { Divider } from '@shoutem/ui'
import { Caption } from '@shoutem/ui'
import { VideoResponse } from '../Components/candidateComponent.js'
import { Actions } from 'react-native-router-flux'

//Sample Response.
var response = {
  "count": 3,
  "next": null,
  "previous": null,
  "results": [
    {
      "id": "ee8d955f-0210-425a-899d-ed51af2c1687",
      "first_name": "Amritanshu",
      "last_name": "Dwivedi",
      "email": "dwivedi.amritanshu@gmail.com",
      "resume": "https://noldevmedia.s3.amazonaws.com/candidate/201701171513111484665991.jpg",
      "linkedin":"https://linkedin.com/adwidevi",
      "angellist": "https://angel.co/grappus",
      "company": "Grappus",
      "open_job": "05822011-fd94-48bd-bf41-cde28594e3cc",
      "created_at": "2017-01-06T08:49:10.459751Z"
    },
  ]
}

export default class CandidateDetail extends Component{
  render(){
    const style ={
      container:{
        alignItems: 'center',
      }
    }
    return(
      <View style={{ paddingBottom: 10 }}>
        <Header style={{ backgroundColor: "#000000" }}>
          <Button transparent>
            <TouchableOpacity onPress={()=>{
              Actions.pop()
                }
              }
            >
              <Icon name='ios-arrow-back' style={{color: 'white'}} />
            </TouchableOpacity>
          </Button>
          <Title>CANDIDATE DETAILS</Title>
        </Header>
        <ScrollView >
          <Card style={styles.card}>
            <CardItem header style={{backgroundColor:'#F0F0F0'}}>
              <Text>
                DETAILS
              </Text>
            </CardItem>
            <CardItem>
              <Text style={styles.title}>{response.results[0].first_name+" "+response.results[0].last_name}</Text>
            </CardItem>
            <CardItem>
              <View style={styles.iconContainer}>
                {
                  response.results[0].linkedin != ""?
                    <SocialIcon raised={ true } type='linkedin' iconSize={18} style={styles.socialButton} />
                  :
                    <SocialIcon raised={ true } type='linkedin' iconSize={18} style={styles.disabledSocialButton} />
                }
                {
                  response.results[0].angellist != ""?
                    <SocialIcon raised={ true } type='angellist' iconSize={18} style={styles.socialButton} />
                  :
                    <SocialIcon raised={ true } type='angellist' iconSize={18} style={styles.disabledSocialButton} />
                }
              </View>
            </CardItem>
            <CardItem>
              <Text style={styles.textStyle}>{response.results[0].email}</Text>
            </CardItem>
          </Card>
          <View style={styles.videoContainer}>
              <VideoResponse />
          </View>
        </ScrollView>
      </View>
    )
  }
}

styles=StyleSheet.create({
  mainContainer:{
    marginTop:80,
    marginBottom:20,
  },
  videoContainer:{
    alignItems: 'center',
    marginBottom: 20,
    marginTop:20
  },
  card:{
    margin: 15
  },
  title:{
    fontSize: 25,
    color:"#000000",
    textAlign: 'center',
  },
  socialButton:{
    backgroundColor: "#000000",
    height:30,
    width:30
  },
  iconContainer:{
    justifyContent:'space-around',
    alignItems: 'center',
    flexDirection:'row',
    marginTop: 10
  },
  textStyle:{
    textAlign: 'center',
  },
  disabledSocialButton:{
    backgroundColor: "#E3E3E3",
    height:30,
    width:30
  }
})


module.exports = {
  CandidateDetail
}
