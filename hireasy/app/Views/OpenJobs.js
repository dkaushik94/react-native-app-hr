// React Imports
import React, { Component } from 'react';
// React native Imports.
import{
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
  AppRegistry,
} from 'react-native';

import { Badge, Button, Container, Content, Header, Icon, Title, List, ListItem, Spinner } from 'native-base'
// import { List,ListItem } from 'react-native-elements'
import Job from '../Components/JobListComponent.js'
import { GlobalFunction, getData, ActivityIcon } from '../globalAssets.js'
import ActionButton from 'react-native-action-button'
import { Actions } from 'react-native-router-flux'



//View for listing of Jobs.
export default class JobListing extends Component{
  constructor(props){
    super(props);
    this.state = {
      dataStream: {},
      // company:'',
      page_no:1
    }
  }

  componentWillMount(){

    let params = {
      'method' : 'GET',
      'routeSuffix' : 'JobsRoute',
      'queryParameters' : {
        'page_no': this.state.page_no,
      }

    }
    var componentRef = this;
    getData(componentRef,params)
  }


  render(){
    const renderJobDetails = (j_Id) => {
      parameters = {sceneName:'JobDetails' , jobId:j_Id}
      GlobalFunction(parameters)
    }
    return(
      <View style={{flex: 1}}>
        <Header style={{ backgroundColor: "#000000" }}>
        <Title>CURRENT JOBS</Title>
        {
          Object.keys(this.state.dataStream).length>0
          ?
          (
          <Button transparent>
            <Badge success style={{backgroundColor:'#7A0520'}}>
              {this.state.dataStream.count}
            </Badge>
          </Button>
          )
          :
          (
            <Button transparent>
              <Badge success style={{backgroundColor:'#7A0520'}}>
                {0}
              </Badge>
            </Button>
          )
        }
        </Header>
        <ScrollView>
          {
            Object.keys(this.state.dataStream).length>0?
            (
                <List>
                  {
                    this.state.dataStream.jobs.map((job)=>{
                      return(
                        <ListItem key={job.id} button onPress={()=>{
                          renderJobDetails(job.id)
                        }}>
                          <View style={{flexDirection: 'column'}}>
                            <Text>{ job.title }</Text>
                            <Text note style = {{fontSize:12}}>{ this.state.dataStream.company.display_name }</Text>
                          </View>
                        </ListItem>
                      )
                    })
                  }
                </List>
            )
          :(
            <ActivityIcon />
          )
        }
        </ScrollView>
        <ActionButton
        backgroundTappable={ true }
        onPress={() => {
          GlobalFunction({sceneName:'AddJob'})
        }
      } />
      </View>
    );
  }
}


//View For job details.
export class JobDetail extends Component{
  render(){
    return(
      <View>
        <Header style={{ backgroundColor: "#000000" }}>
          <Button transparent>
          <TouchableOpacity onPress={()=>{
              Actions.pop()
              }
            }>
            <Icon name='ios-arrow-back' style={{color: 'white'}} />
          </TouchableOpacity>
        </Button>
          <Title>JOB DETAILS</Title>
        </Header>
        <Job id={this.props.jobId}/>
      </View>
    )
  }
}



// AppRegistry.registerComponent('JobListing', () => JobListing)
module.exports = {
  JobDetail,
  JobListing
}
