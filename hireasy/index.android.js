/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Image,
  Navigator,
  TouchableHighlight,
  ToolbarAndroid,
  BackAndroid
} from 'react-native';

import SplashScreen from 'react-native-splash-screen';
import Sidemenu from 'react-native-elements';
import { Actions , Scene , Router } from 'react-native-router-flux';
import { JobListing,JobDetail } from './app/Views/OpenJobs.js';
import { CandidateDetail } from './app/Views/CandidateView.js'
import { AddJob } from './app/Views/AddJobView.js'
import { AddCandidate } from './app/Views/AddCandidateView.js'


const scenes = Actions.create(
    <Scene key="root" hideNavBar={ true }>
      <Scene key="OpenJobs" component={ JobListing } initial={true} title="OPEN JOBS"/>
      <Scene key="CandidateDetails" component={ CandidateDetail } title="CANDIDATE DETAILS"/>
      <Scene key="JobDetails" component={ JobDetail } title="JOB DETAILS"/>
      <Scene key="AddJob" component={ AddJob } title="ADD A JOB" />
      <Scene key="AddCandidate" component={ AddCandidate } title="ADD CANDIDATE" />
    </Scene>
);



// Main Entry point for the application.
export class Hireasy extends Component {

  componentDidMount(){
    setTimeout(()=>{
      SplashScreen.hide();
    },3000)
    // SplashScreen.hide();;
  }

  render()
  {
    return(
        <Router scenes={ scenes } />
    );
  }
}




AppRegistry.registerComponent('hireasy', () => Hireasy);
